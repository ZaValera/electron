import moment from 'moment';

export const DATE_FORMAT = 'DD.MM.YYYY HH:mm:ss';

export const DATE_EXIF_FORMAT = 'YYYY:MM:DD HH:mm:ss';

export interface IElectronAPI {
    openImage: () => Promise<IImageData>;
    changeSize: (name: string) => Promise<string>;
}

export interface IImageData {
    path: string;
    date: string;
    width: number;
    height: number;
}
