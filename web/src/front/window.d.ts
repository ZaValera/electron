import {IElectronAPI} from 'shared/consts';
import moment from 'moment';
import {configureStore} from '@reduxjs/toolkit';


declare global {
    interface Window {
        electronAPI: IElectronAPI;
        moment: typeof moment;
        store: ReturnType<typeof configureStore>;
    }
}
