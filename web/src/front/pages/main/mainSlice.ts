import React from 'react';
import {PayloadAction} from '@reduxjs/toolkit';
import {createUseSliceHook} from 'src/sliceHook';
import {IImageData} from 'shared/consts';

const initialState: ISliceState = {
    imageData: null,
};

const reducers = {
    setImageData(state: ISliceState, action: PayloadAction<ISliceState['imageData']>) {
        state.imageData = action.payload;
    },
};

export function getSliceData() {
    return {
        name: 'main',
        initialState,
        reducers,
    };
}

export const useMainSlice = createUseSliceHook(getSliceData);

export const MainContext = React.createContext<TSlice>(null);

export interface ISliceState {
    imageData: IImageData;
}

type TSlice = ReturnType<typeof useMainSlice>['slice'];
