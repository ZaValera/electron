import React from 'react';
import styles from './mainPage.scss';
import {Stack} from '@mui/material';
import moment from 'moment';
import {MainContext, useMainSlice} from './mainSlice';
import {Controls} from 'src/pages/main/components/controls/Controls';
import {DATE_EXIF_FORMAT, DATE_FORMAT} from 'shared/consts';


export function MainPage() {
    const {slice, state} = useMainSlice();
    const {imageData} = state;

    return (
        <MainContext.Provider value={slice}>
            <Stack
                className={styles.mainPage}
                spacing={2}
            >
                <Controls/>
                {imageData &&
                    <div className={styles.info}>
                        <div>{imageData.path}</div>
                        <div>Date: {moment(imageData.date, DATE_EXIF_FORMAT).format(DATE_FORMAT)}</div>
                        <div>Width: {imageData.width} px</div>
                        <div>Height: {imageData.height} px</div>
                    </div>
                }
                {imageData &&
                    <img
                        alt={imageData.path}
                        className={styles.img}
                        src={`file:///${imageData.path}`}
                    />
                }
            </Stack>
        </MainContext.Provider>
    );
}
