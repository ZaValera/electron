import React, {useState} from 'react';
import styles from './controls.scss';
import {Button, Stack} from '@mui/material';
import {MainContext} from 'src/pages/main/mainSlice';
import {useSlice} from 'src/sliceHook';

export function Controls(props: IProps) {
    const {state, actions} = useSlice(MainContext);
    const {imageData} = state;
    const [status, setStatus] = useState<string>('init');

    return (
        <Stack
            className={styles.buttonRow}
            spacing={2}
            direction={'row'}
        >
            <Button
                size={'small'}
                variant={'contained'}
                onClick={async () => {
                    const data = await window.electronAPI.openImage();

                    if (data) {
                        actions.setImageData(data);
                    }

                    setStatus('init');
                }}
            >
                Open Image
            </Button>
            <Button
                color='success'
                size={'small'}
                variant={'contained'}
                disabled={!imageData}
                onClick={async () => {
                    const data = await window.electronAPI.changeSize(imageData.path);

                    setStatus(data);
                }}
            >
                Change Size to 1000px
            </Button>
            {status !== 'init' &&
                <div
                    className={status === 'error' ? styles.error : styles.success}
                >
                    {status}
                </div>
            }
        </Stack>
    );
}

interface IProps {

}
