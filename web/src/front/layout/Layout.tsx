import React from 'react';
import styles from './layout.scss';
import {Body} from './components/body/Body';
import {Head} from './components/head/Head';

export function LayoutComponent() {
    return (
        <div className={styles.main}>
            <Head/>
            <Body/>
        </div>
    );
}

export const Layout = LayoutComponent;
