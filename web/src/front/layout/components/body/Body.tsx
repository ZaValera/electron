import React, {Suspense} from 'react';
import styles from './body.scss';
import {Router} from 'src/pages/Router';
import {CircularProgress} from '@mui/material';

export function Body() {
    return (
        <div className={styles.body}>
            <Suspense fallback={<CircularProgress/>}>
                <Router/>
            </Suspense>
        </div>
    );
}