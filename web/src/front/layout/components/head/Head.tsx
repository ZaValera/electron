import React, {useState, useRef} from 'react';
import {Route, Link, Routes} from 'react-router-dom';
import {Toolbar, Menu, AppBar, IconButton, Typography, MenuItem} from '@mui/material';
import {Menu as MenuIcon} from '@mui/icons-material';
import styles from './head.scss';

export function Head() {
    const [showMenu, setShowMenu] = useState(false);

    const handleClick = () => {
        setShowMenu(true);
    };

    const handleClose = () => {
        setShowMenu(false);
    };

    const menuButtonRef = useRef(null);

    return (
        <AppBar position="relative" style={{zIndex: 1}}>
            <Toolbar>
                <IconButton edge="start" color="inherit" aria-label="menu" onClick={handleClick} ref={menuButtonRef}>
                    <MenuIcon/>
                </IconButton>
                <Menu
                    anchorEl={menuButtonRef.current}
                    keepMounted
                    open={showMenu}
                    onClose={handleClose}
                    // getContentAnchorEl={null}
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'center',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                    }}
                >
                    <MenuItem>
                        <Link onClick={handleClose} className={styles.link} to='/main'>Main</Link>
                    </MenuItem>
                </Menu>
                <Typography variant="h6" className={styles.title}>
                    <Routes>
                        <Route path='/main' element={<div>Main</div>}/>
                    </Routes>
                </Typography>
            </Toolbar>
        </AppBar>
    );
}
