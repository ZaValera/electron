import {contextBridge, ipcRenderer} from 'electron';


contextBridge.exposeInMainWorld('electronAPI', {
    openImage: () => ipcRenderer.invoke('dialog:openImage'),
    changeSize: (name: string) => ipcRenderer.invoke('changeSize', name),
})