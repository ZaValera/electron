import {writeFile} from 'fs/promises';
import {dialog, IpcMainInvokeEvent} from 'electron';
import sharp from 'sharp';
import {getExifData} from 'src/backUtils';


export async function handleOpenImage() {
    const data = await dialog.showOpenDialog({
        properties: ['openFile'],
    });

    const {canceled, filePaths} = data;

    if (!canceled) {
        const path = filePaths[0];
        const exifData = await getExifData(path);

        return {
            path,
            date: exifData.exif.CreateDate,
            width: exifData.exif.ExifImageWidth,
            height: exifData.exif.ExifImageHeight,
        };
    }
}

export async function handleChangeSize(e: IpcMainInvokeEvent, name: string) {
    try {
        const smallImage = await sharp(name)
            .withMetadata()
            .resize({width: 1000})
            .toBuffer();

        const [main, ext] = name.split('.');
        const newName = `${main}_small.${ext}`

        await writeFile(newName, smallImage);

        return newName;
    } catch (e) {
        return 'error';
    }
}
