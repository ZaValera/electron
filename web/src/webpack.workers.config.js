const path = require('path');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const {getCommonLoaders, getParams} = require('./webpack.utils');

module.exports = env => {
    const {
        isDev,
        dirname,
        mode,
        devtool,
    } = getParams(env);

    return {
        mode,
        devtool,
        target: 'web',
        cache: {
            type: 'filesystem',
            cacheDirectory: path.resolve(dirname, '../webpack_cache'),
        },
        entry: {
            worker: {
                import: './src/workers/someWorker.ts',
                filename: 'workers/someWorker.js',
            },
        },
        output: {
            path: path.resolve(dirname, '../build'),
            publicPath: '/build/',
        },
        module: {
            rules: [
                {
                    test: /\.ts$/,
                    exclude: /node_modules/,
                    use: [
                        ...getCommonLoaders(dirname, isDev),
                        {
                            loader: 'ts-loader',
                            options: {
                                configFile: path.resolve(dirname, 'workers/tsconfig.json'),
                            },
                        },
                    ],
                },
            ],
        },
        resolve: {
            extensions: ['.ts', '.js'],
            alias: {
                shared: path.resolve(dirname, './shared'),
            },
        },
        plugins: [
            new FriendlyErrorsWebpackPlugin(),
        ],
    };
};