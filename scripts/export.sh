#!/bin/bash
RED='\033[0;31m'
NC='\033[0m'

mainDir=$(dirname "$(dirname "$(readlink -f "$0")")")

if ! [ -f "$mainDir"/build_path ]
then
  echo -e "${RED}ERROR: Can't find build_path${NC}"
  exit 1
fi

buildPath=$(cat "$mainDir"/build_path)

if ! [ -d "$buildPath" ]
then
  echo -e "${RED}ERROR: Directory '$buildPath' is not exist${NC}"
  exit 1
fi

export buildPath
