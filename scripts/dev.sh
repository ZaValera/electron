#!/bin/bash

declare -x buildPath

Green='\033[0;32m'
NC='\033[0m'

mainDir=$(dirname "$(dirname "$(readlink -f "$0")")")
. "$mainDir"/scripts/export.sh || exit 1

rm -rf "${buildPath:?}"/devBuild || exit 1
mv "${mainDir:?}"/tmp "$buildPath"/devBuild
echo -e "${Green}Finished successfully ${NC}"